
start: install
	docker-compose up --detach
	@printf "starting up"; while [ "`docker-compose ps|grep 'health: starting'`" ]; do printf .; sleep 1; done; echo "everything's running"
	docker-compose exec backend bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration

stop:
	docker-compose down --remove-orphans

log:
	docker-compose logs --tail=$(if $(TAIL),$(TAIL),4) --follow

test: install
	docker-compose run --rm --no-deps backend bin/fastest $(FILTER)

install: backend/config/jwt backend/vendor docker-compose.log

deploy-ready: install
	docker-compose run --rm --no-deps backend bin/console cache:clear --env=lambda
	docker-compose run --rm --no-deps frontend yarn build

# real targets

backend/config/jwt: | backend/vendor
	docker-compose run --rm --no-deps backend bin/console lexik:jwt:generate-keypair --skip-if-exists
	touch -c $@

backend/vendor: backend/composer.json $(wildcard backend/composer.lock) | docker-compose.log
	docker-compose run --rm --no-deps backend composer install
	touch -c $@

frontend/node_modules: frontend/package.json $(wildcard frontend/yarn.lock) | docker-compose.log
	docker-compose run --rm --no-deps frontend yarn install
	touch -c $@

# cheated: since i don't have a way to figure out that the build is current just create a log file
docker-compose.log: backend/Dockerfile
	docker-compose build |tee docker-compose.log
