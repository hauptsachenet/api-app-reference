import {useEffect, useState} from "react";

let isInitialRendering = true;

/**
 * This function returns if the current rendering process is the initial one.
 *
 * @return {boolean}
 */
export function useInitialRendering(): boolean {
    const [initialRendering, setInitialRendering] = useState(isInitialRendering);

    useEffect(() => {
        isInitialRendering = false;
        setInitialRendering(false);
    }, []);

    return initialRendering;
}
