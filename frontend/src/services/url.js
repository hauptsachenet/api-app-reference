/**
 * This function is intended as a template tag function.
 * All parameters will be passed though {@see encodeURIComponent}.
 *
 * @example url\`/my/${path}\`
 * @param {string[]} fragments
 * @param {string|number} parameters
 * @return {string}
 */
export function url(fragments, ...parameters) {
    const furtherFragments = parameters.map((parameter, index) => {
        return encodeURIComponent(parameter) + fragments[index + 1];
    });

    return fragments[0] + furtherFragments.join('');
}

/**
 * This is the same as {@see url} but checks all parameter types.
 * If any parameter isn't a string or number, then this entire function will return null.
 * This is important so you can use it in useSWR or other react hooks.
 *
 * @example useSWR(urlOrNull\`/product/${dynamicId}\`)
 * @param {string[]} fragments
 * @param {any} parameters
 * @return {null|string}
 */
export function urlOrNull(fragments, ...parameters) {
    const criteria = parameter => typeof parameter === 'string' || typeof parameter === 'number';
    if (!parameters.every(criteria)) {
        return null;
    }

    return url(fragments, ...parameters);
}
