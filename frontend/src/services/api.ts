import {useInitialRendering} from "./state";
import jwtDecode from "jwt-decode";
import {useEffect} from "react";
import useSWR, {cache, mutate, SWRConfiguration} from "swr";
import {createLocalStorageStateHook} from "use-local-storage-state";
import {useRouter} from "next/router";
import {UseFormReturn} from "react-hook-form/dist/types";

const useLoginToken = createLocalStorageStateHook<string>('user');

export interface Token {
  iat: number;
  exp: number;
  roles: string[];
  username: string;
}

export interface Login {
  roles: string[];
  token: string | undefined;
  setToken: (token: string) => void
}

/**
 * Use (and validate) the login.
 */
export function useLogin(expectRole?: string[]): Login {
  const [token, setToken] = useLoginToken();
  const initialRendering = useInitialRendering();
  const auth = typeof token === "string" ? jwtDecode(token) as Token : undefined;
  const timeToExpire = auth?.exp ? (auth.exp * 1000 - Date.now()) : 0;

  // clear the token after the timeout
  useEffect(() => {
    if (timeToExpire > 0) {
      const timeout = setTimeout(() => setToken(undefined), timeToExpire);
      return () => clearTimeout(timeout);
    }
  }, [timeToExpire]);

  // if some auth is required, prepare redirect when auth is missing
  if (expectRole && expectRole.length > 0) {
    const router = useRouter();
    useEffect(() => {
      const loginInvalid = timeToExpire < 0
          || !expectRole.every(role => auth?.roles?.includes(role));
      if (loginInvalid) {
        router.push(`/login`);
      }
    }, [router, auth?.roles, timeToExpire])
  }

  // the initial render step must be equal to the server side rendering
  if (initialRendering || !auth || timeToExpire <= 0) {
    return {token: undefined, setToken, roles: []};
  }

  return {token, setToken, roles: auth.roles};
}

/**
 * Use a get resource as react hook
 */
export function useResource(url: string | null, token?: string, options: SWRConfiguration = {}) {
  const swrKey = url && token !== null ? [url, token] : null;
  const {data, error} = useSWR(swrKey, api, options);
  return {data, error, loading: !data && !error};
}

/**
 * Maps errors from api platform on form
 */
export function mapErrors(error: any, form: UseFormReturn) {
  if (error && Array.isArray(error.violations)) {
    for (const {propertyPath, message} of error.violations) {
      form.setError(propertyPath, {type: "manual", message});
    }
  } else {
    throw error;
  }
}

export type SupportedMethod = 'POST' | 'PUT' | 'GET';

/**
 * Runs a get request to the api.
 *
 * @example useSWR([url`/api/products/${id}`, token], fetcher)
 */
export async function api(url: string, token?: string, method: SupportedMethod = 'GET', data?: Record<string, any>) {
  const headers: Record<string, string> = {
    'Accept': 'application/ld+json, application/json',
  };

  if (data) {
    headers['Content-Type'] = 'application/ld+json; charset=utf-8';
  }

  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }

  const body = data ? JSON.stringify(data) : undefined;
  const response = await fetch(url, {method, headers, body});

  switch (response.status) {

    case 200:
    case 201:
      const newData = await response.json();
      if (method !== 'GET') {
        cache.clear();
        if (typeof newData?.['@id'] === 'string') {
          await mutate([newData['@id'], token], newData);
        }
      }
      return newData;

    case 204:
      cache.clear();
      return {};

    default:
      throw await response.json();
  }
}
