import {useRouter} from "next/router";
import {useForm} from "react-hook-form";
import BasicLayout from "../components/BasicLayout";
import {FormInput} from "../components/Form";
import {api, useLogin} from "../services/api";

export default function Login() {
  const {token, setToken} = useLogin();
  const router = useRouter();
  if (token) {
    router.push(`/users`);
  }

  const form = useForm();

  const login = async values => {
    const response = await api(`/api/login`, token, 'POST', values);
    setToken(response.token);
    await router.push(`/users`);
  };

  return <BasicLayout>
    <form onSubmit={form.handleSubmit(login)}>
      <FormInput form={form} name="username"/>
      <FormInput form={form} name="password" type="password"/>
      <button type="submit" disabled={form.formState.isSubmitting}>
        Login
      </button>
    </form>
  </BasicLayout>;
}
