import Head from "next/head";
import {useRouter} from "next/router";
import {useForm} from "react-hook-form";
import BasicLayout from "../../components/BasicLayout";
import {FormButton, FormInput} from "../../components/Form";
import {api, mapErrors, useLogin} from "../../services/api";
import {urlOrNull} from "../../services/url";

export default function UserNew() {
  const {token} = useLogin(["ROLE_ADMIN"]);
  const router = useRouter();
  const form = useForm();

  const submit = async data => {
    try {
      await api(urlOrNull`/api/users`, token, 'POST', data);
      await router.push('/users');
    } catch (e) {
      mapErrors(e, form);
    }
  };

  return <BasicLayout>

    <Head>
      <title>User {form.watch('email') ?? '[unnamed]'}</title>
    </Head>

    <h1>User {form.watch('email') ?? '[unnamed]'}</h1>

    <form onSubmit={form.handleSubmit(submit)}>
      <FormInput form={form} name="email" options={{required: 'Provide an email'}}/>
      <FormInput form={form} name="newPassword" type="password"/>
      <FormButton form={form}>Save</FormButton>
      <FormButton onClick={() => router.push(`/users`)}>Back</FormButton>
    </form>

  </BasicLayout>;
}
