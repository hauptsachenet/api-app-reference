import Head from "next/head";
import Link from "next/link";
import BasicLayout from "../../components/BasicLayout";
import {useLogin, useResource} from "../../services/api";

export default function UserList() {
  const {token} = useLogin(["ROLE_USER"]);
  const {data} = useResource('/api/users', token);

  return <BasicLayout>
    <Head>
      <title>User list</title>
    </Head>
    <h1>User list</h1>

    <ul>
      {data?.['hydra:member']?.map(user => (
          <li key={user.id}>
            <Link href={`/users/${user.id}`}>
              {user.email}
            </Link>
          </li>
      ))}
    </ul>

    <Link href="/users/new">Add user</Link>

  </BasicLayout>;
}
