import {ErrorMessage} from "@hookform/error-message";
import {useFormState} from "react-hook-form";
import styles from "./Form.module.css"

export function FormInput({
  form,
  name,
  options = {},
  type = 'text',
  label = name,
  id = name.replace(/\W/g, '_'),
  disabled = false,
  ...rest
}) {
  const {isSubmitting, errors} = useFormState({control: form.control, name});

  return <div className={styles.group}>
    <label className={styles.label} htmlFor={id}>
      {label}
    </label>
    <input className={styles.field}
           type={type}
           id={id}
           disabled={isSubmitting || disabled}
           {...rest}
           {...form.register(name, {setValueAs: emptyStringAsNull, ...options})}/>
    <ErrorMessage errors={errors} name={name} render={({message}) => <p>{message}</p>}/>
  </div>;
}

export function FormButton({
  form,
  children,
  type = form ? "submit" : "button",
  disabled = false,
  ...rest
}) {
  return <button type={type} disabled={form?.formState?.isSubmitting || disabled} {...rest}>
    {children}
  </button>;
}

/**
 * Ensures an empty string is treated as null.
 * This is the expected behavior in symfony models.
 *
 * @param {*} value
 * @return {string|null}
 */
function emptyStringAsNull(value) {
  if (typeof value === 'string' && value.length > 0) {
    return value;
  }

  return null;
}
