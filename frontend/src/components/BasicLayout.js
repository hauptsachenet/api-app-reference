import {useLogin} from "../services/api";

export default function BasicLayout ({children}) {
  const {token, setToken} = useLogin();

  return <>
    <div>You are: {token ? 'somebody' : 'nobody'}</div>
    {token &&
      <div><a href="#" onClick={() => setToken(null)}>Logout</a></div>
    }
    <main>{children}</main>
    <div>This is some fine footer</div>
  </>
}
