const {PHASE_DEVELOPMENT_SERVER} = require('next/constants');

module.exports = phase => ({

  reactStrictMode: true,

  async rewrites() {
    const rewrites = [];

    if (phase === PHASE_DEVELOPMENT_SERVER) {
      rewrites.push({
        // forward api requests to avoid CORS
        // https://stackoverflow.com/a/65467719/1973256
        source: '/api/:path*',
        destination: `${process.env.API_URL}/api/:path*`, // Proxy to Backend
      });
      rewrites.push({
        source: '/bundles/:path*',
        destination: `${process.env.API_URL}/bundles/:path*`, // Proxy to Backend
      });
      rewrites.push({
        source: '/media/:path*',
        destination: `${process.env.API_URL}/media/:path*`, // Proxy to Backend
      });
    }

    return rewrites;
  },
});
