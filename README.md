Symfony/Next reference project
==============================

This project shows how to setup a symfony project for api only usage
and then how to use that api in a next backend application. 


how to work
-----------

### create the initial user

Make sure that you configured your own `JWT_PASSPHRASE`.

Then use the command `user:add` to create the initial users.

### create additional api resources

Just create new doctrine entities and add the `ApiResource` attribute.
The default serialization and security settings for any ApiResource are defined in `config/packages/api_resource.yaml`.

### create a crud

TODO

### send mails

look at `Symfony\Bridge\Twig\Mime\NotificationEmail` and
`backend/vendor/symfony/twig-bridge/Resources/views/Email/zurb_2/notification/body.html.twig`
for reference as to how to template emails.


Setup steps from scratch (for repetition and documentation)
------------------------

- `git init` is always a good start

### `backend/` (Symfony + ApiPlatform)

- `symfony new backend --no-git` to create the backend structure
- `composer require 'php:8.0.*' api cli http-client jwt mailer vich/uploader-bundle`
    - `'php:8.0.*'` locks the compatible php version to `8.0.*`
    - `api` to add the api pack and doctrine with all dependencies
    - `cli` add console command support (should be installed by default but just in case)
    - `http-client` to make http calls (and for NotCompromisedPasswordValidator)
    - `jwt` to add jwt auth to the project
    - `mailer` to add the mailer pack
    - `vich/uploader-bundle` to handle file uploads
- `composer require --dev profiler`
   - `profile ` to get the `/_profiler` in dev environment
- add `docker-compose.yml` to the root and configure the `backend` service
- add `Makefile` to the root and configure `start`, `install`, `backend/vendor` and `backend/config/jwt` jobs
- configure database  
    - configure `doctrine.orm.mapping.type=attribute` in `backend/config/doctrine.yaml`
    - add a `mysql:5.7` `database` service in `docker-compose.yml`
    - configure `DATABASE_URL=mysql://root:password@db:3306/database?serverVersion=5.7` for `backend` in `docker-compose.yml`
- configure validation 
    - enable auto_mapping validation in `backend/config/packages/validator.yaml`
    - create the `ValidatorPropertyMetadataFactory` to extend the open api property specification with validation
- configure user authentication
    - create the `backend/src/Entity/User.php` entity that implements `UserInterface`
    - create password encoding using `UserPasswordListener` and `EntityListenerInterface` and configure them in `services.yaml` 
    - configure `api_keys` and `defaults` in  `backend/config/packages/api_platform.yaml`
    - configure the user in`providers`, `encoders`, `filewalls.api` in `backend/config/security.yaml`
    - configure the fake `api_login` route in `backend/config/routes.yaml` that leads to the login path in the `security.yaml`
    - configure the login schema in swagger using the `backend/src/OpenApi/OpenApiLoginSchema.php`
      and the required `OpenApiDecorator` in order to not create another decorator for each schema addition
      which you also need to configure in `backend/config/service.yaml`
    - add the `backend/src/Service/Security/AdminVoter` for the "admin can do anything by default" rule.
- configure mailing
    - create a `.env` variable `MAILER_FROM=noreply@hauptsache.net`
    - configure the default from header in `backend/config/packages/mailer.yaml`
    - you probably also want `composer require league/html-to-markdown twig/cssinliner-extra twig/inky-extra`
        - `league/html-to-markdown` is used by `Symfony\Bridge\Twig\Mime\BodyRenderer::convertHtmlToText` if it exists
        - `twig/cssinliner-extra` and `twig/inky-extra` to enable `{% apply inky_to_html|inline_css %}` in twig


### `frontend/` (next + api integration)

- install next: https://nextjs.org/learn/basics/create-nextjs-app/setup
- move the `pages` folder into the `src` folder
- remove the default page and `Home.module.css`

