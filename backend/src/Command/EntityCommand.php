<?php

namespace App\Command;

use ApiPlatform\Core\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class EntityCommand extends Command
{
    public function __construct(
        protected ValidatorInterface $apiValidator,
        protected EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    protected function findOneBy(string $class, array $criteria): object
    {
        $object = $this->entityManager->getRepository($class)->findOneBy($criteria);
        if (!$object) {
            /** @noinspection JsonEncodingApiUsageInspection */
            throw new \RuntimeException("$class not found with: " . json_encode($criteria));
        }

        return $object;
    }

    protected function persistAndFlush(OutputInterface $output, object ...$entities): void
    {
        $meta = [];

        foreach ($entities as $index => $entity) {
            $this->apiValidator->validate($entity);
            $meta[$index] = [$entity, !$this->entityManager->contains($entity)];
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();

        foreach ($meta as [$entity, $wasNew]) {
            $output->writeln($wasNew ? "$entity created." : "$entity updated.");
        }
    }

    protected function getValue(string $property, InputInterface $input, OutputInterface $output): string
    {
        $value = $input->getOption($property);
        if (!empty($value)) {
            return $value;
        }

        $question = new Question("$property: ");
        $question->setHidden(preg_match('#password|secret#i', $property));
        $value = $this->getHelper('question')->ask($input, $output, $question);
        if (!empty($value)) {
            return $value;
        }

        throw new \RuntimeException("No value for $property.");
    }
}
