<?php


namespace App\Command\User;


use App\Command\EntityCommand;
use App\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserPasswordCommand extends EntityCommand
{
    protected function configure()
    {
        $this->setName('user:password');
        $this->addArgument('id', InputArgument::REQUIRED);
        $this->addOption('password', null, InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var User $user */
        $user = $this->findOneBy(User::class, ['id' => $input->getArgument('id')]);
        $user->newPassword = $this->getValue('password', $input, $output);
        $this->persistAndFlush($output, $user);
        return 0;
    }
}
