<?php

namespace App\Command\User;

use App\Command\EntityCommand;
use App\Entity\Region;
use App\Entity\User;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserAddCommand extends EntityCommand
{
    protected function configure()
    {
        $this->setName('user:add');
        $this->addOption('email', null, InputOption::VALUE_REQUIRED);
        $this->addOption('password', null, InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User();
        $user->email = $this->getValue('email', $input, $output);
        $user->newPassword = $this->getValue('password', $input, $output);
        $this->persistAndFlush($output, $user);
        return 0;
    }
}
