<?php

namespace App\Service;

/**
 * This is just a maker interface to allow autowire-ing the tag onto given services.
 *
 * This isn't necessary if you don't have any dependencies but you will have some...
 *
 * You'll also still need the ORM\EntityListeners({"className"}) annotation.
 *
 * @see \Doctrine\ORM\Mapping\EntityListeners
 */
interface EntityListenerInterface
{

}
