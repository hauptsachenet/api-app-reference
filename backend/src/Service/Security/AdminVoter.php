<?php

namespace App\Service\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * This voter will just say "yes" to everything as long as you have "ROLE_ADMIN".
 * It is an easy starting point to add authorization, even if you don't have a concept yet.
 */
class AdminVoter implements VoterInterface
{
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        if (!in_array('ROLE_ADMIN', $token->getRoleNames(), true)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        return VoterInterface::ACCESS_GRANTED;
    }
}
