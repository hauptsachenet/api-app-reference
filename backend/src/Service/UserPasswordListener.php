<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPasswordListener implements EntityListenerInterface
{
    public function __construct(
        private UserPasswordHasherInterface $passwordEncoder
    ) {

    }

    public function preFlush(User $user, PreFlushEventArgs $event)
    {
        if ($user->newPassword === null) {
            return;
        }

        $user->password = $this->passwordEncoder->hashPassword($user, $user->newPassword);
        $user->eraseCredentials();
    }
}
