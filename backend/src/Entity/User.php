<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Service\UserPasswordListener;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\EntityListeners([UserPasswordListener::class])]
#[ApiResource]
class User implements UserInterface, PasswordAuthenticatedUserInterface, EquatableInterface
{
    use EntityFields;

    // FIXME user can edit his email
    #[ORM\Column(length: 50, unique: true)]
    #[Assert\Email]
    #[Serializer\Groups(["list", "read", "write"])]
    public string $email;

    #[ORM\Column(nullable: true)]
    #[ApiProperty(writable: false)]
    public ?string $password = null;

    #[Serializer\Groups(["write"])]
    #[Assert\Length(min: 6, max: 32)]
    #[Assert\NotCompromisedPassword]
    public ?string $newPassword = null;

    public function getRoles(): array
    {
        // adjust to your needs
        return ['ROLE_USER', 'ROLE_ADMIN'];
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
        $this->newPassword = null;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof self) {
            return false;
        }

        return $user->id === $this->id;
    }
}
