<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

trait EntityFields
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(options: ["unsigned" => true])]
    #[Serializer\Groups(["read", "list"])]
    public ?int $id = null;

    /**
     * The data type.
     *
     * This method emulates the behavior of the {@link InheritanceType} dtype property.
     * If you select using {@link Query::getArrayResult()}, you'll get a property dtype as well, even without this.
     * The capitalisation is intentional to make the object and the array compatible in twig:
     * "{{ object.dtype }}" should work with the object as well as the array result.
     *
     * @return string
     */
    public function getDtype(): string
    {
        $shortClassName = substr(strrchr(get_class($this), '\\'), 1);
        return mb_strtolower($shortClassName);
    }

    /**
     * Returns an actually unique name by combining the class name and the id.
     *
     * @return string
     */
    public function getUniqueId(): string
    {
        $identifier = $this->id ?: ('#' . spl_object_id($this));
        return "{$this->getDtype()}:$identifier";
    }

    /**
     * This should output a string that resembles the entity best.
     * It should only be used for debugging though.
     *
     * You may override it.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getUniqueId();
    }
}
