<?php

namespace App\ApiPlatform\OpenApi;

use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\OpenApi;
use App\ApiPlatform\OpenApiSchemaInterface;
use Symfony\Component\Routing\RouterInterface;

class OpenApiLoginSchema implements OpenApiSchemaInterface
{
    private const TOKEN_SCHEMA_NAME = 'Token';
    public const TOKEN_SCHEMA_REF = '#/components/schemas/' . self::TOKEN_SCHEMA_NAME;

    public function __construct(
        private RouterInterface $router
    ) {

    }

    public function extendApiDocumentation(OpenApi $openApi)
    {
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas[self::TOKEN_SCHEMA_NAME] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['Credentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'johndoe@example.com',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'password',
                ],
            ],
        ]);

        // @formatter:off
        $pathItem = new Model\PathItem(
            ref: 'JWT Token',
            post: new Model\Operation(
                operationId: 'postCredentialsItem',
                tags: ['Token'],
                responses: [
                    '200' => [
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => self::TOKEN_SCHEMA_REF,
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'Username + Password login',
                requestBody: new Model\RequestBody(
                    description: 'The JWT Token',
                    content: new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        // @formatter:on

        $openApi->getPaths()->addPath($this->router->generate('api_login'), $pathItem);
    }
}
