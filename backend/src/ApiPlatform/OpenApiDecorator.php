<?php

namespace App\ApiPlatform;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;

class OpenApiDecorator implements OpenApiFactoryInterface
{
    /**
     * @param OpenApiFactoryInterface $decorated
     * @param OpenApiSchemaInterface[] $openApiSchemas
     */
    public function __construct(
        private OpenApiFactoryInterface $decorated,
        private iterable $openApiSchemas
    ) {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);

        foreach ($this->openApiSchemas as $schema) {
            $schema->extendApiDocumentation($openApi);
        }

        return $openApi;
    }
}
