<?php


namespace App\ApiPlatform;


use ApiPlatform\Core\OpenApi\OpenApi;

/**
 * This interface allows to easily extend the OpenApi definition fo ApiPlatform.
 *
 * Use it directly where you define routes, at best directly in your controller.
 */
interface OpenApiSchemaInterface
{
    public function extendApiDocumentation(OpenApi $openApi);
}
