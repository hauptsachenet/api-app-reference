<?php

namespace App\ApiPlatform;

use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Property\PropertyMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Exception\NoSuchMetadataException;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * This extends the api platform property metadata with more specific validator schemas.
 * Api Platform already adds some validator info the the schema,
 * {@see \ApiPlatform\Core\Bridge\Symfony\Validator\Metadata\Property\ValidatorPropertyMetadataFactory}.
 * But if falls short when it comes to specifics like string lengths and enums.
 *
 * The validations should be json schema definitions
 * {@see https://json-schema.org/understanding-json-schema/reference/index.html}.
 */
class ValidatorPropertyMetadataFactory implements PropertyMetadataFactoryInterface
{
    public function __construct(
        private PropertyMetadataFactoryInterface $decorated,
        private ValidatorInterface $validator
    ) {

    }

    public function create(string $resourceClass, string $property, array $options = []): PropertyMetadata
    {
        $propertyMetadata = $this->decorated->create($resourceClass, $property, $options);

        try {
            /** @var ClassMetadata $validatorClass */
            $validatorClass = $this->validator->getMetadataFor($resourceClass);
            $validatorProperties = $validatorClass->getPropertyMetadata($property);
            $openApiContext = $propertyMetadata->getAttribute('openapi_context') ?? [];
            foreach ($validatorProperties as $validatorProperty) {
                foreach ($validatorProperty->getConstraints() as $constraint) {
                    if (!in_array('Default', $constraint->groups, true)) {
                        continue;
                    }

                    $openApiContext += match (true) {

                        $constraint instanceof Assert\Length && !$constraint->allowEmptyString => array_filter([
                            'minLength' => $constraint->min,
                            'maxLength' => $constraint->max,
                        ], 'is_numeric'),

                        // TODO this must be tested before if the pattern is compatible
                        // $constraint instanceof Assert\Regex => [
                        //     'pattern' => $constraint->getHtmlPattern(),
                        // ],

                        $constraint instanceof Assert\Range && !$constraint->minPropertyPath && !$constraint->maxPropertyPath => [
                            'minimum' => $constraint->min,
                            'maximum' => $constraint->max,
                        ],

                        $constraint instanceof Assert\Choice && $constraint->choices => [
                            'enum' => $constraint->choices,
                        ],

                        default => [],
                    };
                }
            }

            if (!empty($openApiContext)) {
                $attributes = $propertyMetadata->getAttributes();
                $attributes['openapi_context'] = $openApiContext;
                $propertyMetadata = $propertyMetadata->withAttributes($attributes);
            }
        } catch (NoSuchMetadataException $e) {
            // ignore
        }
        return $propertyMetadata;
    }
}
